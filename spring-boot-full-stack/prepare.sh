#!/bin/bash

function handleError () {
    exitCode=$?
    if [ $exitCode -eq "0" ]
     then
        echo "Previous command exited just fine."
     else
        echo "$0 exited unexpectedly with status: $exitCode"
        exit 1
     fi
}

pushd polling-app-server
echo "Building server component"
./mvnw -DskipTests=true package >> ../prepare.log
handleError
popd

echo "Installing NPM dependencies"
pushd polling-app-client
npm install >> ../prepare.log
handleError
echo "Building production version"
npm run build >> ../prepare.log
handleError
popd

echo "You can now setup mysql according to instructions."
echo docker run --name mysql-srv -e MYSQL_ROOT_PASSWORD=toor -d mysql:latest
echo docker run -it --rm mysql mysql -hmysql-srv -uroot -ptoor
echo "See: Readme.md "


