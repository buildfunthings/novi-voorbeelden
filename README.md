# Voorbeeld projecten voor NOVI studenten

## Spring Boot / MVC

Petclinic: https://github.com/spring-projects/spring-petclinic

## Software Development Praktijk 2: Spring Boot MVC

Bron: https://springframework.guru/spring-boot-web-application-part-1-spring-initializr/

Bevat de volgende onderdelen:

- Spring Boot
- Spring Security
- Spring MVC
- Thymeleaf templates
- Hibernate

Documentatie: [README](spring-boot-mvc/README.md "README")

Additionele functionaliteiten bovenop de originele versie:

- maven wrapper toegevoegd

## Software Development Praktijk 3: Spring Boot Full Stack

Bron: https://www.callicoder.com/spring-boot-spring-security-jwt-mysql-react-app-part-4/

Bevat de volgende onderdelen:

- Spring Boot 
- Spring Security 
- JWT 
- MySQL 
- React

Documentatie: [README](spring-boot-full-stack/Readme.md "README")

Additionele scripts die ik heb gemaakt:

- prepare.sh : bouw alle onderdelen en geeft instructies over docker gebruik
